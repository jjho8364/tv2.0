package com.innovalic.finalfreetv.model;

/**
 * Created by Administrator on 2016-07-02.
 */
public class GridViewItem {

    public String ImgUrl;
    public String title;
    public String listUrl;
    public String updateDt;

    public GridViewItem(){

    }

    public GridViewItem(String imgUrl, String title, String listUrl, String updateDt) {
        this.ImgUrl = imgUrl;
        this.title = title;
        this.listUrl = listUrl;
        this.updateDt = updateDt;
    }

    public String getUpdateDt() {
        return updateDt;
    }

    public void setUpdateDt(String updateDt) {
        this.updateDt = updateDt;
    }

    public String getImgUrl() {
        return ImgUrl;
    }

    public void setImgUrl(String imgUrl) {
        ImgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getListUrl() {
        return listUrl;
    }

    public void setListUrl(String listUrl) {
        this.listUrl = listUrl;
    }
}
