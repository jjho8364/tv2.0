package com.innovalic.finalfreetv.model;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-12.
 */
public class GetEnglandListARr {
    private String TAG = "GetEnglandListARr - ";
    ArrayList<ListViewItem2> listArr;


    public ArrayList<ListViewItem2> getListArr(String title) {
        ArrayList<ListViewItem2> listArr = new ArrayList<ListViewItem2>();
        Log.d(TAG, "title : " + title);
        switch (title) {
            case "닥터후 시즌1" :
                listArr.add(new ListViewItem2("닥터후 시즌1 - 01화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/52/20101102062052680prfshv0xj7a1a.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 02화", "http://183.110.26.43/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/70/201011020627104155sth4krtt8k74.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 03화", "http://183.110.26.78/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/80/201011020627330718rlbjzxwyxv30.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 04화", "http://183.110.26.48/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/96/2010110206275914977os5gjx0ftm3.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 05화", "http://183.110.26.43/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/05/20101102062847946jkuoj3evh6pzl.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 06화", "http://183.110.26.42/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/02/20101102062052712s4gmaxscsoxl7.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 07화", "http://183.110.26.44/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/03/20101102062153712pg5rm1je5i355.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 08화", "http://183.110.26.81/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/09/20101102062639774j628x3xwojfyc.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 09화", "http://183.110.25.106/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/13/20101102062609399ismf2gsa9k4zp.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 10화", "http://183.110.25.98/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/14/20101102062820165fsghcoekv8gc9.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 11화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/20/20101102062819212zvcxd9gc59c17.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 12화", "http://183.110.26.49/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/22/201011020628214627ucusbw6jggmd.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌1 - 13화 완결", "http://183.110.26.77/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/24/20101102062821165qvct2l9cbi0om.flv"));
                break;
            case "닥터후 시즌2" :
                listArr.add(new ListViewItem2("닥터후 시즌2 - 01화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=2Sq8JbsnzQI$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 02화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=9XoCsXkR508$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 03화", "http://183.110.26.90/redirect/cmvs.mgoon.com/storage2/m2_video/2010/08/31/4091263.mp4?px-bps=1385829&px-bufahead=10"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 04화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=ZDLbDSIYoVg$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 05화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=wcfbGqBXWUA$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 06화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=0XeFNvG2s6c$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 07화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=51mTbbPiSK8$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 08화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=u59WN-VM9Sg$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 09화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=Zu1XSDK0qFc$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 10화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=QHbBhXKVd9A$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 11화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=upbzVrA2dJk$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 12화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=74EWwOaaO0A$"));
                listArr.add(new ListViewItem2("닥터후 시즌2 - 13화", "http://videofarm.daum.net/controller/video/viewer/Video.html?play_loc=tvpot&jsCallback=true&vid=zwIpR7dAd7g$"));
                break;
            case "닥터후 시즌3" :
                listArr.add(new ListViewItem2("닥터후 시즌3 - 01화", "http://183.110.25.109/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/48/201011010326395240mjm5qroezf3j.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 02화", "http://183.110.25.107/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/60/20101101032616509622osc0g1bs6a.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 03화", "http://183.110.25.102/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/71/20101101032617368s591udn5n1zi0.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 04화", "http://183.110.25.112/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/90/20101101032627290lavqi4nmfvqlc.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 05화", "http://183.110.26.81/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/30/20101028094042059lvglsref8gcbd.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 06화", "http://183.110.26.78/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/42/20101029054303371ry1vormdpmtpi.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 07화", "http://183.110.26.79/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/54/20101029054241871rww2isbyx1yqr.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 08화", "http://183.110.26.81/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/66/20100926064207251vmzxfx8dkxv5w.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 09화", "http://183.110.26.46/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/61/201009260649134549q1bd7setgtgy.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 10화", "http://183.110.26.76/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/66/201010290554380436067z5v4ds30w.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 11화", "http://183.110.26.80/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/81/20101029055458871idpvouyrzj4ro.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 12화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/00/20101029055441340gjlqgmtjq4yli.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌3 - 13화 완결", "http://183.110.25.100/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/flv/_user/e/h/ehddnr4349/24/20101029055442121ixr0pg9u4dcpt.flv"));
                break;
            case "닥터후 시즌9" :
                listArr.add(new ListViewItem2("닥터후 시즌9 - 01화", "http://183.110.25.98/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/p/r/proshocker/79/20150922145640369zosw0h6zg86qx.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 02화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/u/dudem/82/20150927155628374lc6wvjpdwtqzr.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 03화", "http://183.110.25.100/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/p/r/proshocker/62/20151004131654148vhplg2afdseff.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 04화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/84/201510120625369193wjfezu5ukfwj.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 05화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/p/r/proshocker/90/20151018145700000t0j5apbwirwdt.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 06화", "http://183.110.26.83/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/27/201510260937547490dddfafn1kbf5.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 07화", "http://183.110.26.79/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/d/o/doctorwho8/27/20151108165755779vict6n3m3rvwl.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 08화", "http://183.110.26.78/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/34/20151110212455179wbojkkbfv66ss.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 09화", "http://183.110.26.49/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/15/20151123061709623ebv1hrbz6juct.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 10화", "http://183.110.25.96/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/17/20151123061948349txxd71u3fgq43.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 11화", "http://trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/34/20151130065212189omdtriwotfjfx.flv"));
                listArr.add(new ListViewItem2("닥터후 시즌9 - 12화", "http://183.110.25.104/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/79/20151207060229328jczji0n06zzng.flv"));
                listArr.add(new ListViewItem2("닥터후 크리스마스 스페셜", "http://183.110.25.98/redirect/trans-idx.cdn2.pandora.tv/apple2.pandora.tv/hd/_user/a/s/asd8888/16/20151227122640516weyx8p6hrojt0.flv"));

                break;
        }


        return listArr;
    }

}
