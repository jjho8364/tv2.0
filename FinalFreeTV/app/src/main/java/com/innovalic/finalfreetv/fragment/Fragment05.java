package com.innovalic.finalfreetv.fragment;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;

import com.innovalic.finalfreetv.R;
import com.innovalic.finalfreetv.activity.MainActivity;
import com.innovalic.finalfreetv.activity.MidListActivity;
import com.innovalic.finalfreetv.adapter.GridViewAdapter;
import com.innovalic.finalfreetv.adapter.GridViewMidAdapter;
import com.innovalic.finalfreetv.model.GridViewItemMid;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Administrator on 2016-07-02.
 */
public class Fragment05 extends Fragment implements View.OnClickListener {
    private final String TAG = " Fragment05 - ";
    private GridView gridView;
    private ArrayList<GridViewItemMid> gridArr;

    private Button frBtnHistory;

    private EditText editText;
    private GridViewMidAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;

        // history show
        ((MainActivity)getActivity()).showHistoryBtn();
        int portrait_width_pixel= Math.min(this.getResources().getDisplayMetrics().widthPixels, this.getResources().getDisplayMetrics().heightPixels);
        int dots_per_virtual_inch=this.getResources().getDisplayMetrics().densityDpi;
        boolean isPhone = true;
        boolean contactsFlag = false;
        //boolean phoneNumFlag = false;
        boolean usimFlag = false;
        float virutal_width_inch=portrait_width_pixel/dots_per_virtual_inch;
        if (virutal_width_inch <= 2) { isPhone = true; } else { isPhone = false; }
        Cursor c = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        int contactsCnt = c.getCount();
        if(contactsCnt < 20){ contactsFlag = true; }
        TelephonyManager telManager = (TelephonyManager)getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        //String phoneNum = telManager.getLine1Number();
        //if(phoneNum == null || phoneNum.equals("")){ phoneNumFlag = true; }
        //if(operator == null || operator.equals("")){ operatorFlag = true; }
        if (telManager.getSimState() == TelephonyManager.SIM_STATE_ABSENT){
            Log.d(TAG, "유심 x");
            usimFlag = true;
        } else {
            Log.d(TAG, "유심 o");
            usimFlag = false;
        }

        Log.d(TAG, "contactsFlag : " + contactsFlag);
        Log.d(TAG, "usimFlag : " + usimFlag);
        Log.d(TAG, "isPhone : " + isPhone);
        //Log.d(TAG, "phoneNumFlag : " + phoneNumFlag);
        if ((contactsFlag || usimFlag) && isPhone) {
            view = inflater.inflate(R.layout.depend, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment05, container, false);
            gridArr = new ArrayList<GridViewItemMid>();
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/ugc?t=470x180&q=http://blogfiles.naver.net/20160711_83/shineheather_1468210967149UiC2M_GIF/fa3df330881473_5637a6bcf087c.gif","베르사유 시즌1"));
            gridArr.add(new GridViewItemMid("http://blogfiles13.naver.net/20160706_12/tigrmetal2_1467771779996j3rc4_JPEG/van_helsing_xlg.jpg","반 헬싱 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/45/86/79/57_2458679_poster_image_1466676545626.jpg","슈퍼걸 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/171/171596.jpg","왕좌의게임 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=nct68x86&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/210/210267.jpg","왕좌의게임 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/19/22/57_671922_poster_image_1399878875193.jpg","왕좌의게임 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/36/78/57_673678_poster_image_1400647658628.jpg","왕좌의게임 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/97/72/36/57_1977236_poster_image_1425015822193.jpg","왕좌의게임 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/23/87/28/57_3238728_poster_image_1455587605718.jpg","왕좌의게임 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/50/85/57_665085_poster_image_1446615648972.jpg","워킹데드 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/82/06/57_668206_poster_image_1446615616022.jpg","워킹데드 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/02/58/57_670258_poster_image_1446615546556.jpg","워킹데드 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/39/05/57_673905_poster_image_1446615515538.jpg","워킹데드 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/70/28/53/57_1702853_poster_image_1446615469842.jpg","워킹데드 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/21/78/94/57_2217894_poster_image_1446615218285.jpg","워킹데드 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/50/46/97/57_2504697_poster_image_1437549109838.jpg","피어 더 워킹데드 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/07/43/77/57_3074377_poster_image_1460344494595.jpg","피어 더 워킹데드 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/32/55/57_663255_poster_image_1403673813076.jpg","굿 와이프 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/41/92/57_664192_poster_image_1403674662577.jpg","굿 와이프 시즌2"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/83/57/57_668357_poster_image_1403673876955.jpg","굿 와이프 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/40/93/57_674093_poster_image_1403674364922.jpg","굿 와이프 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/92/94/13/57_1929413_poster_image_1456469605676.jpg","굿 와이프 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/63/46/57_2656346_poster_image_1431411234297.jpg","굿 와이프 시즌7"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/154/154468.jpg","히어로즈 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=nct68x86&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/154/154469.jpg","히어로즈 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=nct68x86&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/154/154470.jpg","히어로즈 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=nct68x86&q=http://sstatic.naver.net/keypage/image/dss/57/66/31/30/57_663130_poster_image_1398862062225.jpg","히어로즈 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?quality=8&expire=24&origin=yes&size=c82x82&q=http%3A%2F%2Fpost.phinf.naver.net%2F20150820_18%2Fdoridory00_1440020394261eujXW_JPEG%2Fmug_obj_144002039310959565.jpg%3Ftype%3Df218_218","히어로즈 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/85/81/71/57_1858171_poster_image_1417488930802.jpg","고담 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/46/46/44/57_2464644_poster_image_1440057376325.jpg","고담 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/31/24/86/57_2312486_poster_image_1429679323376.jpg","데어데블 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/51/89/64/57_2518964_poster_image_1453106601294.jpg","데어데블 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/10/92/02/57_3109202_poster_image_1455757680551.jpg","바이닐 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/15/02/81/57_3150281_poster_image_1450688575616.jpg","샨나라 연대기 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/20/75/57_662075_poster_image_1400144850968.jpg","빅뱅 이론 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/23/48/57_662348_poster_image_1400144782515.jpg","빅뱅 이론 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/31/16/57_663116_poster_image_1400144890713.jpg","빅뱅 이론 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/42/84/57_664284_poster_image_1400145313706.jpg","빅뱅 이론 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/83/67/57_668367_poster_image_1400145387976.jpg","빅뱅 이론 시즌5"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/11/16/57_671116_poster_image_1400145421047.jpg","빅뱅 이론 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/35/31/57_673531_poster_image_1400145603558.jpg","빅뱅 이론 시즌7"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/94/66/31/57_1946631_poster_image_1445584543203.jpg","빅뱅 이론 시즌8"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/62/66/93/57_2626693_poster_image_1430295952788.jpg","빅뱅 이론 시즌9"));

            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=nf120x150&q=http%3A%2F%2Fsstatic.naver.net%2Fkeypage%2Fimage%2Fdss%2F57%2F67%2F86%2F76%2F57_2678676_poster_image_1438149595315.jpg","리미트리스 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/51/70/86/57_2517086_poster_image_1453683959779.jpg","루시퍼 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/62/66/21/57_2626621_poster_image_1430293191077.jpg","주(Zoo) 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/63/10/96/57_3631096_poster_image_1465461156655.jpg","주(Zoo) 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/00/95/88/57_2009588_poster_image_1418021720794.jpg","플래시 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/31/11/01/57_2311101_poster_image_1458119095526.jpg","플래시 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=nf120x150&q=http%3A%2F%2Fsstatic.naver.net%2Fkeypage%2Fimage%2Fdss%2F57%2F67%2F32%2F13%2F57_2673213_poster_image_1441178164665.jpg","마이너리티 리포트 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/50/74/16/57_2507416_poster_image_1446605190894.jpg","익스팬스"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/68/36/54/57_2683654_poster_image_1453339779162.jpg","DC 레전드 오브 투모로우"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/21/85/57_662185_poster_image_1400049246548.jpg","프린지 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/31/35/57_663135_poster_image_1400049648970.jpg","프린지 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/43/14/57_664314_poster_image_1400049630413.jpg","프린지 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/02/48/12/57_2024812_poster_image_1433124470364.jpg","에이전트 카터 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/04/49/57_2650449_poster_image_1453165831833.jpg","에이전트 카터 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/30/21/32/57_2302132_poster_image_1420511695300.jpg","12몽키즈 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/44/62/01/57_2446201_poster_image_1460941664041.jpg","12몽키즈 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/169/169965.jpg","리스너 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/153/153197.jpg","리스너 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/181/181228.jpg","리스너 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/46/61/57_674661_poster_image_1404715600734.jpg","리스너 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/05/77/57_660577_poster_image_1400051800672.jpg","그레이 아나토미 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/09/52/57_660952_poster_image_1400053419749.jpg","그레이 아나토미 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/13/22/57_661322_poster_image_1400053438997.jpg","그레이 아나토미 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/31/17/57_663117_poster_image_1400052536806.jpg","그레이 아나토미 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/50/35/57_665035_poster_image_1400053478102.jpg","그레이 아나토미 시즌7"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/79/01/57_667901_poster_image_1400053554567.jpg","그레이 아나토미 시즌8"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/11/61/57_671161_poster_image_1399967168664.jpg","그레이 아나토미 시즌9"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/35/25/57_673525_poster_image_1400052613505.jpg","그레이 아나토미 시즌10"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/92/94/18/57_1929418_poster_image_1418109986259.jpg","그레이 아나토미 시즌11"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/62/48/57_2656248_poster_image_1443050582964.jpg","그레이 아나토미 시즌12"));
            gridArr.add(new GridViewItemMid("http://post.phinf.naver.net/20151231_46/14515720046354i1uR_JPEG/%B3%F4%C0%BA_%BC%BA%C0%C7_%BB%E7%B3%AA%C0%CC01.jpg?type=w1200","높은 성의 사나이 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/27/27/57_672727_poster_image_1400141485009.jpg","바이킹스 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/34/53/57_673453_poster_image_1455674579570.jpg","바이킹스 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/01/22/29/57_2012229_poster_image_1427273788377.jpg","바이킹스 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/46/76/45/57_2467645_poster_image_1455676031692.jpg","바이킹스 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/28/24/57_672824_poster_image_1446194763820.jpg","언더 더 돔 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/09/41/76/57_2094176_poster_image_1437459540910.jpg","언더 더 돔 시즌3"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/43/15/57_664315_poster_image_1447058325165.jpg","캐슬 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/08/88/57_670888_poster_image_1447056471785.jpg","캐슬 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/41/52/57_674152_poster_image_1449295925928.jpg","캐슬 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/05/91/22/57_2059122_poster_image_1429074417844.jpg","캐슬 시즌7"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/38/17/57_2653817_poster_image_1440488417704.jpg","캐슬 시즌8"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/30/53/57_663053_poster_image_1415680868428.jpg","Glee 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/50/26/57_665026_poster_image_1415681199468.jpg","Glee 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/79/26/57_667926_poster_image_1415680895114.jpg","Glee 시즌3"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/07/47/57_670747_poster_image_1415680909097.jpg","Glee 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/39/50/57_673950_poster_image_1415681608070.jpg","Glee 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/44/45/57_674445_poster_image_1420782327773.jpg","Glee 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/83/30/57_668330_poster_image_1429668188532.jpg","그림형제 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/07/46/57_670746_poster_image_1400056968041.jpg","그림형제 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/35/32/57_673532_poster_image_1400053794756.jpg","그림형제 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/92/94/97/57_1929497_poster_image_1429668516324.jpg","그림형제 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/45/86/17/57_2458617_poster_image_1444192254644.jpg","그림형제 시즌5"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/53/34/51/57_3533451_poster_image_1461660037584.jpg","그림형제 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/84/02/57_668402_poster_image_1400139351910.jpg","원스 어폰 어 타임 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/34/98/57_673498_poster_image_1400138825220.jpg","원스 어폰 어 타임 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/99/10/26/57_1991026_poster_image_1418023484077.jpg","원스 어폰 어 타임 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/41/81/57_2654181_poster_image_1460107333212.jpg","원스 어폰 어 타임 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/155/155275.jpg","NCIS 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/155/155276.jpg","NCIS 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/155/155283.jpg","NCIS 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/08/16/57_660816_poster_image_1398059537608.jpg","NCIS 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/13/16/57_661316_poster_image_1398059702637.jpg","NCIS 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/215/215540.jpg","NCIS 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/155/155287.jpg","NCIS 시즌7"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/155/155288.jpg","NCIS 시즌8"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/184/184161.jpg","NCIS 시즌9"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/226/226740.jpg","NCIS 시즌10"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/35/11/57_673511_poster_image_1400751086267.jpg","NCIS 시즌11"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/02/48/34/57_2024834_poster_image_1418016806435.jpg","NCIS 시즌12"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/76/39/57_2657639_poster_image_1450852331502.jpg","NCIS 시즌13"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/198/198036.jpg","NCIS 로스엔젤레스 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/155/155274.jpg","NCIS 로스엔젤레스 시즌2"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/174/174759.jpg","NCIS 로스엔젤레스 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/185/185717.jpg","NCIS 로스엔젤레스 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/35/10/57_673510_poster_image_1399965186365.jpg","NCIS 로스엔젤레스 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/08/14/44/57_2081444_poster_image_1417761436420.jpg","NCIS 로스엔젤레스 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/81/52/57_2658152_poster_image_1431496769926.jpg","NCIS 로스엔젤레스 시즌7"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/02/50/45/57_2025045_poster_image_1418016740406.jpg","NCIS 뉴올리언스 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/31/24/41/57_2312441_poster_image_1459839751070.jpg","NCIS 뉴올리언스 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/01/79/48/57_2017948_poster_image_1419574198731.jpg","CSI 시즌15"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/63/10/34/57_3631034_poster_image_1465459667705.jpg","오렌지 이즈 더 뉴 블랙 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/70/12/72/57_3701272_poster_image_1465968813085.jpg","스트레인저 씽즈 시즌1"));
            gridArr.add(new GridViewItemMid("http://v.hanmaer.com/image/pic/201607/thumb/12532.jpg","바이스 프린서플 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/32/64/57_663264_poster_image_1399514398947.jpg","모던 패밀리 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/41/91/57_664191_poster_image_1399514493923.jpg","모던 패밀리 시즌2"));
            //gridArr.add(new GridViewItemMid("","모던 패밀리 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/183/183234.jpg","모던 패밀리 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/35/90/57_673590_poster_image_1399514946249.jpg","모던 패밀리 시즌5"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/01/78/85/57_2017885_poster_image_1427709306280.jpg","모던 패밀리 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/38/24/57_2653824_poster_image_1442474841469.jpg","모던 패밀리 시즌7"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/87/50/57_658750_poster_image_1437532244211.jpg","CSI 마이애미 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/216/216247.jpg","CSI 마이애미 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/212/212426.jpg","CSI 마이애미 시즌7"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/11/01/57_661101_poster_image_1400065022333.jpg","CSI 뉴욕 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/216/216275.jpg","CSI 뉴욕 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/13/19/57_661319_poster_image_1452661461319.jpg","CSI 뉴욕 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/24/03/57_662403_poster_image_1400065231544.jpg","CSI 뉴욕 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/154/154490.jpg","CSI 뉴욕 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/42/43/57_664243_poster_image_1460184502399.jpg","CSI 뉴욕 시즌7"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/06/72/57_660672_poster_image_1400066931950.jpg","CSI 라스베가스 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/03/78/57_660378_poster_image_1400067825365.jpg","CSI 라스베가스 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/02/77/57_660277_poster_image_1400068020437.jpg","CSI 라스베가스 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/03/47/57_660347_poster_image_1400068452287.jpg","CSI 라스베가스 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/95/89/57_659589_poster_image_1400064204479.jpg","CSI 라스베가스 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/93/32/57_659332_poster_image_1400064487771.jpg","CSI 라스베가스 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/07/81/57_660781_poster_image_1400068588631.jpg","CSI 라스베가스 시즌7"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/193/193074.jpg","CSI 라스베가스 시즌8"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/23/78/57_662378_poster_image_1400068859387.jpg","CSI 라스베가스 시즌9"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/32/08/57_663208_poster_image_1400064039952.jpg","CSI 라스베가스 시즌10"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/41/96/57_664196_poster_image_1400067178565.jpg","CSI 라스베가스 시즌11"));
            //gridArr.add(new GridViewItemMid("","CSI 라스베가스 시즌12"));
            //gridArr.add(new GridViewItemMid("","CSI 라스베가스 시즌13"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/35/26/57_673526_poster_image_1397114576330.jpg","CSI 라스베가스 시즌14"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/01/79/48/57_2017948_poster_image_1419574198731.jpg","CSI 라스베가스 시즌15"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/22/91/16/57_2229116_poster_image_1423537736658.jpg","CSI 사이버 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/82/71/57_2658271_poster_image_1456208630868.jpg","CSI 사이버 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/46/33/64/57_2463364_poster_image_1452822780342.jpg","X-파일 시즌10"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/81/14/57_668114_poster_image_1400148380347.jpg","슈츠 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/95/89/57_669589_poster_image_1401090976411.jpg","슈츠 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/33/32/57_673332_poster_image_1400146720279.jpg","슈츠 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/69/23/22/57_1692322_poster_image_1429509819605.jpg","슈츠 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/03/95/21/57_2039521_poster_image_1433149588186.jpg","슈츠 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/43/24/57_674324_poster_image_1447227044393.jpg","투모로우 피플 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/52/35/31/57_3523531_poster_image_1461220219062.jpg","섀도우 헌터스 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/33/84/57_663384_poster_image_1398833510404.jpg","화이트칼라 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/41/41/57_664141_poster_image_1398833524841.jpg","화이트칼라 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/78/66/57_667866_poster_image_1398833839566.jpg","화이트칼라 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/03/90/57_670390_poster_image_1398833874423.jpg","화이트칼라 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/34/84/57_673484_poster_image_1460184132801.jpg","화이트칼라 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/01/58/49/57_2015849_poster_image_1449294253014.jpg","화이트칼라 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/01/07/57_660107_poster_image_1400638442405.jpg","프리즌 브레이크 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/09/18/57_660918_poster_image_1400638473585.jpg","프리즌 브레이크 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/13/06/57_661306_poster_image_1400638529653.jpg","프리즌 브레이크 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/23/06/57_662306_poster_image_1400638590900.jpg","프리즌 브레이크 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/06/83/57_670683_poster_image_1446795765110.jpg","애로우 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/38/37/57_673837_poster_image_1446794796161.jpg","애로우 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/88/40/93/57_1884093_poster_image_1446794553313.jpg","애로우 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/31/24/59/57_2312459_poster_image_1441871732914.jpg","애로우 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/23/39/57_662339_poster_image_1399702638332.jpg","브레이킹 배드 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/36/28/57_663628_poster_image_1399702951667.jpg","브레이킹 배드 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/36/29/57_663629_poster_image_1399703132990.jpg","브레이킹 배드 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/166/166283.jpg","브레이킹 배드 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/94/48/57_669448_poster_image_1399703837969.jpg","브레이킹 배드 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/83/61/57_2658361_poster_image_1459832034622.jpg","더 플레이어 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/22/69/57_662269_poster_image_1398318243689.jpg","프렌즈 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/22/69/57_662269_poster_image_1398318243689.jpg","프렌즈 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/22/69/57_662269_poster_image_1398318243689.jpg","프렌즈 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/22/69/57_662269_poster_image_1398318243689.jpg","프렌즈 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/22/69/57_662269_poster_image_1398318243689.jpg","프렌즈 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/22/69/57_662269_poster_image_1398318243689.jpg","프렌즈 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/22/69/57_662269_poster_image_1398318243689.jpg","프렌즈 시즌7"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/22/69/57_662269_poster_image_1398318243689.jpg","프렌즈 시즌8"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/22/69/57_662269_poster_image_1398318243689.jpg","프렌즈 시즌9"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/22/69/57_662269_poster_image_1398318243689.jpg","프렌즈 시즌10"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?quality=8&size=f160x160&q=http://dbscthumb.phinf.naver.net/3989_000_1/20150715200703993_LQZ4JGZ4W.jpg/57_661084_poster.jpg?type=w450_fst#120x172","슈퍼 내츄럴 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/13/26/57_661326_poster_image_1446624715831.jpg","슈퍼 내츄럴 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/23/51/57_662351_poster_image_1446624640585.jpg","슈퍼 내츄럴 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/30/52/57_663052_poster_image_1446624541649.jpg","슈퍼 내츄럴 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/41/89/57_664189_poster_image_1446624408578.jpg","슈퍼 내츄럴 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/83/24/57_668324_poster_image_1446623821587.jpg","슈퍼 내츄럴 시즌7"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/10/87/57_671087_poster_image_1446622810895.jpg","슈퍼 내츄럴 시즌8"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/35/27/57_673527_poster_image_1446622540296.jpg","슈퍼 내츄럴 시즌9"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/95/82/21/57_1958221_poster_image_1430958408021.jpg","슈퍼 내츄럴 시즌10"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/31/24/14/57_2312414_poster_image_1442475634587.jpg","슈퍼 내츄럴 시즌11"));

            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/10/85/57_671085_poster_image_1398667618516.jpg","크리미널 마인드 시즌8"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/35/68/57_673568_poster_image_1385958009065.jpg","크리미널 마인드 시즌9"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/93/20/09/57_1932009_poster_image_1440656271892.jpg","크리미널 마인드 시즌10"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/82/48/57_2658248_poster_image_1460107951466.jpg","크리미널 마인드 시즌11"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/27/86/57_672786_poster_image_1424157264863.jpg","오펀 블랙 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/42/96/57_674296_poster_image_1424157470248.jpg","오펀 블랙 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/08/55/73/57_2085573_poster_image_1427180086026.jpg","오펀 블랙 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/191/191894.jpg","뉴스룸 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/226/226211.jpg","뉴스룸 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/43/98/57_674398_poster_image_1418720890624.jpg","뉴스룸 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/31/44/57_663144_poster_image_1400141830434.jpg","뱀파이어 다이어리 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/42/62/57_664262_poster_image_1400141843775.jpg","뱀파이어 다이어리 시즌2"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/83/25/57_668325_poster_image_1400141869340.jpg","뱀파이어 다이어리 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/08/89/57_670889_poster_image_1421205853757.jpg","뱀파이어 다이어리 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/34/50/57_673450_poster_image_1421204751767.jpg","뱀파이어 다이어리 시즌5"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/94/66/22/57_1946622_poster_image_1421204385676.jpg","뱀파이어 다이어리 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/31/21/04/57_2312104_poster_image_1444104468609.jpg","뱀파이어 다이어리 시즌7"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/04/56/57_660456_poster_image_1399872507225.jpg","안투라지 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/41/17/57_664117_poster_image_1399872538256.jpg","안투라지 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/41/17/57_664117_poster_image_1399872538256.jpg","안투라지 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/41/17/57_664117_poster_image_1399872538256.jpg","안투라지 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/41/17/57_664117_poster_image_1399872538256.jpg","안투라지 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/41/17/57_664117_poster_image_1399872538256.jpg","안투라지 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/41/17/57_664117_poster_image_1399872538256.jpg","안투라지 시즌7"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/41/17/57_664117_poster_image_1399872538256.jpg","안투라지 시즌8"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/03/97/88/57_2039788_poster_image_1463536321186.jpg","페이킹잇 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/52/17/53/57_2521753_poster_image_1429770254145.jpg","페이킹잇 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/158/158438.jpg","섹스 앤 더 시티 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/158/158438.jpg","섹스 앤 더 시티 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/158/158438.jpg","섹스 앤 더 시티 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/158/158438.jpg","섹스 앤 더 시티 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/158/158438.jpg","섹스 앤 더 시티 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/158/158438.jpg","섹스 앤 더 시티 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=f120x171&quality=10&q=http://movie.phinf.naver.net/20111221_178/13244635706925qWPF_JPEG/movie_image.jpg?type=w640_2","내가 널 사랑할 수 없는 10가지 이유"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/97/57/57_669757_poster_image_1416988367028.jpg","어쿼드 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/97/57/57_669757_poster_image_1416988367028.jpg","어쿼드 시즌3"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/97/57/57_669757_poster_image_1416988367028.jpg","어쿼드 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/23/40/57_662340_poster_image_1400063023227.jpg","90210 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/31/23/57_663123_poster_image_1400062735733.jpg","90210 시즌2"));
            //gridArr.add(new GridViewItemMid("","90210 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/41/92/57_674192_poster_image_1446461411242.jpg","오리지널 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/94/66/42/57_1946642_poster_image_1418109022035.jpg","오리지널 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/31/20/25/57_2312025_poster_image_1461056043107.jpg","오리지널 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/43/23/57_674323_poster_image_1399965726144.jpg","the 100 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/97/42/20/57_1974220_poster_image_1421105873974.jpg","the 100 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/31/20/24/57_2312024_poster_image_1453338908158.jpg","the 100 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/31/66/57_673166_poster_image_1399529813202.jpg","퍼셉션 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/44/36/57_674436_poster_image_1418193840970.jpg","퍼셉션 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/04/82/62/57_2048262_poster_image_1447232751595.jpg","아이좀비 시즌1"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/03/90/57_2650390_poster_image_1441871617070.jpg","아이좀비 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/99/52/97/57_1995297_poster_image_1404969550040.jpg","더 나이트 쉬프트 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/99/92/40/57_1999240_poster_image_1424662141205.jpg","더 나이트 쉬프트 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/99/45/02/57_1994502_poster_image_1458875284726.jpg","라스트쉽 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/00/86/42/57_2008642_poster_image_1428653989983.jpg","라스트쉽 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/94/64/72/57_2946472_poster_image_1441595387119.jpg","라스트쉽 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=nf120x150&q=http%3A%2F%2Fsstatic.naver.net%2Fkeypage%2Fimage%2Fdss%2F57%2F69%2F51%2F64%2F57_2695164_poster_image_1433143163945.jpg","비트윈 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/153/153837.jpg","하와이 파이브 오 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/227/227187.jpg","하와이 파이브 오 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/10/65/57_671065_poster_image_1400635181317.jpg","하와이 파이브 오 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/35/47/57_673547_poster_image_1456469919998.jpg","하와이 파이브 오 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/96/14/42/57_1961442_poster_image_1446796847669.jpg","하와이 파이브 오 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/63/97/57_2656397_poster_image_1461056401587.jpg","하와이 파이브 오 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/41/45/21/57_2414521_poster_image_1446602487988.jpg","제시카 존스 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/23/73/57_662373_poster_image_1456641647777.jpg","트루 블러드 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/162/162214.jpg","트루 블러드 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/40/55/57_664055_poster_image_1400046455106.jpg","트루 블러드 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/81/26/57_668126_poster_image_1456640912840.jpg","트루 블러드 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/226/226419.jpg","트루 블러드 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/40/12/57_674012_poster_image_1404435541315.jpg","트루 블러드 시즌7"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/24/15/57_662415_poster_image_1399430019469.jpg","멘탈리스트 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/30/56/57_663056_poster_image_1399430318581.jpg","멘탈리스트 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/156/156635.jpg","멘탈리스트 시즌3"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/83/54/57_668354_poster_image_1439974449643.jpg","멘탈리스트 시즌4"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/202/202925.jpg","멘탈리스트 시즌5"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/40/94/57_674094_poster_image_1397107717521.jpg","멘탈리스트 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/93/20/44/57_1932044_poster_image_1446692499559.jpg","멘탈리스트 시즌7"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/02/68/15/57_2026815_poster_image_1417774310540.jpg","Z네이션 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/21/14/08/57_2211408_poster_image_1418028983987.jpg","Z네이션 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/209/209536.jpg","한니발 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/37/78/57_673778_poster_image_1393985536939.jpg","한니발 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/96/56/05/57_1965605_poster_image_1433296443364.jpg","한니발 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/55/77/19/57_3557719_poster_image_1462512834541.jpg","아웃캐스트 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/51/46/58/57_2514658_poster_image_1444113319568.jpg","프리처 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/34/04/57_673404_poster_image_1447228593161.jpg","에이전트 오브 쉴드 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/95/65/29/57_1956529_poster_image_1418108823630.jpg","에이전트 오브 쉴드 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/95/65/29/57_1956529_poster_image_1418108823630.jpg","에이전트 오브 쉴드 시즌3"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/63/89/57_2656389_poster_image_1443079302468.jpg","에이전트 오브 쉴드 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/62/05/39/57_2620539_poster_image_1431681824967.jpg","다크매터 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/62/05/39/57_2620539_poster_image_1431681824967.jpg","다크매터 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/06/27/00/57_3062700_poster_image_1452586110117.jpg","콜로니 시즌1"));

            //gridArr.add(new GridViewItemMid("","시카고 파이어 시즌1"));
            //gridArr.add(new GridViewItemMid("","시카고 파이어 시즌2"));
            //gridArr.add(new GridViewItemMid("","시카고 파이어 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/46/56/94/57_2465694_poster_image_1442560624960.jpg","시카고 파이어 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/226/226540.jpg","고스트 위스퍼러 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/154/154135.jpg","고스트 위스퍼러 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/210/210874.jpg","고스트 위스퍼러 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/202/202585.jpg","고스트 위스퍼러 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/31/85/57_663185_poster_image_1398313328335.jpg","고스트 위스퍼러 시즌5"));
            gridArr.add(new GridViewItemMid("http://postfiles10.naver.net/data32/2008/7/2/25/12_skideff.jpg?type=w2","배틀스타 갤럭티카 시즌0"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/52/75/57_665275_poster_image_1399862353318.jpg","배틀스타 갤럭티카 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/08/44/57_660844_poster_image_1399862288743.jpg","배틀스타 갤럭티카 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/15/01/57_661501_poster_image_1399862237755.jpg","배틀스타 갤럭티카 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/25/61/57_662561_poster_image_1399862179484.jpg","배틀스타 갤럭티카 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/02/50/72/57_2025072_poster_image_1418016535053.jpg","스콜피온 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/02/50/72/57_2025072_poster_image_1418016535053.jpg","스콜피온 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/05/63/33/57_3056333_poster_image_1453684419957.jpg","더 매지션스 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/44/44/57_674444_poster_image_1400396064045.jpg","브룩클린 나인나인 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/88/30/18/57_1883018_poster_image_1418111688076.jpg","브룩클린 나인나인 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/31/80/48/57_2318048_poster_image_1438933010415.jpg","브룩클린 나인나인 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/201/201857.jpg","넘버스 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/154/154293.jpg","넘버스 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/154/154294.jpg","넘버스 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/21/75/57_662175_poster_image_1399022261576.jpg","넘버스 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/154/154296.jpg","넘버스 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/154/154296.jpg","넘버스 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=nf120x150&q=http%3A%2F%2Fsstatic.naver.net%2Fkeypage%2Fimage%2Fdss%2F57%2F01%2F01%2F13%2F57_2010113_poster_image_1421200861506.jpg","콘스탄틴 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/168/168674.jpg","뉴걸 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/07/60/57_670760_poster_image_1399443581482.jpg","뉴걸 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/35/12/57_673512_poster_image_1400054476748.jpg","뉴걸 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/87/85/47/57_1878547_poster_image_1416901949479.jpg","뉴걸 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/47/98/77/57_2479877_poster_image_1451956689198.jpg","뉴걸 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/01/39/57_660139_poster_image_1400747218246.jpg","성범죄수사대:SVU 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/10/26/57_661026_poster_image_1400747417923.jpg","성범죄수사대:SVU 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/154/154671.jpg","성범죄수사대:SVU 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/13/71/57_661371_poster_image_1400747535709.jpg","성범죄수사대:SVU 시즌4"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/89/35/57_658935_poster_image_1400747800712.jpg","성범죄수사대:SVU 시즌5"));
            //gridArr.add(new GridViewItemMid("","성범죄수사대:SVU 시즌6"));
            //gridArr.add(new GridViewItemMid("","성범죄수사대:SVU 시즌7"));
            //gridArr.add(new GridViewItemMid("","성범죄수사대:SVU 시즌8"));
            //gridArr.add(new GridViewItemMid("","성범죄수사대:SVU 시즌9"));
            //gridArr.add(new GridViewItemMid("","성범죄수사대:SVU 시즌10"));
            //gridArr.add(new GridViewItemMid("","성범죄수사대:SVU 시즌11"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/50/36/57_665036_poster_image_1400748137803.jpg","성범죄수사대:SVU 시즌12"));
            //gridArr.add(new GridViewItemMid("","성범죄수사대:SVU 시즌13"));
            //gridArr.add(new GridViewItemMid("","성범죄수사대:SVU 시즌14"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/35/67/57_673567_poster_image_1400748683123.jpg","성범죄수사대:SVU 시즌15"));
            //gridArr.add(new GridViewItemMid("","성범죄수사대:SVU 시즌16"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/49/09/17/57_2490917_poster_image_1440056943201.jpg","성범죄수사대:SVU 시즌17"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/57/77/57_665777_poster_image_1446457285417.jpg","쉐임리스 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/95/20/57_669520_poster_image_1446458678007.jpg","쉐임리스 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/03/08/57_670308_poster_image_1446455888721.jpg","쉐임리스 시즌3"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/98/09/62/57_1980962_poster_image_1446460009524.jpg","쉐임리스 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/06/75/57_670675_poster_image_1400652231701.jpg","캐리 다이어리 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/38/49/57_673849_poster_image_1400652242843.jpg","캐리 다이어리 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/83/94/57_668394_poster_image_1445329319967.jpg","투 브로크 걸즈 시즌1"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/83/94/57_668394_poster_image_1445329319967.jpg","투 브로크 걸즈 시즌1"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/83/94/57_668394_poster_image_1445329319967.jpg","투 브로크 걸즈 시즌1"));


            // 영드
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/65/95/54/57_659554_poster_image_1423901379202.jpg","닥터후 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/04/42/57_660442_poster_image_1423902318828.jpg","닥터후 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/16/08/57_661608_poster_image_1423905198660.jpg","닥터후 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/21/44/57_662144_poster_image_1423905907004.jpg","닥터후 시즌4"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/36/44/57_663644_poster_image_1423906863771.jpg","닥터후 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/45/07/57_664507_poster_image_1423980505582.jpg","닥터후 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/11/14/57_671114_poster_image_1423981685556.jpg","닥터후 시즌7"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/35/58/57_673558_poster_image_1423982486763.jpg","닥터후 시즌8"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/31/24/93/57_2312493_poster_image_1438917002746.jpg","닥터후 시즌9"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/73/15/43/57_1731543_poster_image_1400724406150.jpg","하우스 오브 카드 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/73/15/48/57_1731548_poster_image_1439967848170.jpg","하우스 오브 카드 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/86/58/78/57_1865878_poster_image_1425007468819.jpg","하우스 오브 카드 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/48/54/57_664854_poster_image_1424154401712.jpg","셜록 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=nct68x86&q=http://sstatic.naver.net/keypage/image/dss/57/67/31/58/57_673158_poster_image_1435114809818.jpg","셜록 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=nct68x86&q=http://sstatic.naver.net/keypage/image/dss/57/66/94/76/57_669476_poster_image_1424155154272.jpg","셜록 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/30/00/57_673000_poster_image_1434697272851.jpg","마이 매드 팻 다이어리 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/86/45/96/57_1864596_poster_image_1417576716682.jpg","마이 매드 팻 다이어리 시즌2"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/22/53/32/57_2225332_poster_image_1417576243108.jpg","마이 매드 팻 다이어리 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/19/36/57_661936_poster_image_1401689467998.jpg","스킨스 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/22/53/57_662253_poster_image_1401689565649.jpg","스킨스 시즌2"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/26/75/57_662675_poster_image_1401689627853.jpg","스킨스 시즌3"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/36/57/57_663657_poster_image_1401689702728.jpg","스킨스 시즌4"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/155/155737.jpg","스킨스 시즌5"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/171/171676.jpg","스킨스 시즌6"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/180/180693.jpg","스킨스 시즌7"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/27/12/57_662712_poster_image_1401437163269.jpg","마법사 멀린 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/155/155074.jpg","마법사 멀린 시즌2"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/88/31/57_668831_poster_image_1401771817864.jpg","마법사 멀린 시즌3"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/66/88/32/57_668832_poster_image_1401771904933.jpg","마법사 멀린 시즌4"));
            //gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/176/176470.jpg","마법사 멀린 시즌5"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/67/46/49/57_674649_poster_image_1403232833883.jpg","피키 블라인더스 시즌1"));
            gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/18/45/62/57_2184562_poster_image_1415770061757.jpg","피키 블라인더스 시즌2"));

            gridView = (GridView)view.findViewById(R.id.gridview);
            adapter = new GridViewMidAdapter(getActivity(), gridArr, R.layout.gridviewitem_mid);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getActivity(), MidListActivity.class);
                    intent.putExtra("title", gridArr.get(position).getTitle());
                    intent.putExtra("type", "mid");
                    startActivity(intent);
                }
            });

            editText = (EditText)view.findViewById(R.id.anilist_edit);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String text = editText.getText().toString().toLowerCase(Locale.getDefault());
                    adapter.filter(text);
                }
            });

        }

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
