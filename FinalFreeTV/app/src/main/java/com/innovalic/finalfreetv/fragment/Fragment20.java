package com.innovalic.finalfreetv.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;


import com.innovalic.finalfreetv.R;
import com.innovalic.finalfreetv.activity.MainActivity;
import com.innovalic.finalfreetv.activity.VideoListActivity;
import com.innovalic.finalfreetv.adapter.GridViewAdapter;
import com.innovalic.finalfreetv.model.GridViewItem;
import com.innovalic.finalfreetv.utils.NetworkUtil;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-03.
 */
public class Fragment20 extends Fragment implements View.OnClickListener {
    private final String TAG = " Fragment20 - ";
    private ProgressDialog mProgressDialog;
    private ArrayList<GridViewItem> gridArr;
    private final String baseUrl = "http://www.marutv.com";

    private String attrUrl = "/?s=";
    private String searchWord = "";
    private String searchPagingUrl = "/page/";

    private GridView gridView;
    private GetGridView getGridView = null;

    private EditText editText;
    private Button searchBtn;

    private int pageNum = 1;
    private TextView tv_currentPage;
    private TextView tv_lastPage;
    private Button nextBtn;
    private Button preBtn;


    InputMethodManager inputManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        ((MainActivity)getActivity()).hideHistoryBtn(); // history hide
        int portrait_width_pixel= Math.min(this.getResources().getDisplayMetrics().widthPixels, this.getResources().getDisplayMetrics().heightPixels);
        int dots_per_virtual_inch=this.getResources().getDisplayMetrics().densityDpi;
        boolean isPhone = true;
        boolean contactsFlag = false;
        //boolean phoneNumFlag = false;
        boolean usimFlag = false;
        float virutal_width_inch=portrait_width_pixel/dots_per_virtual_inch;
        if (virutal_width_inch <= 2) { isPhone = true; } else { isPhone = false; }
        Cursor c = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        int contactsCnt = c.getCount();
        if(contactsCnt < 20){ contactsFlag = true; }
        TelephonyManager telManager = (TelephonyManager)getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        //String phoneNum = telManager.getLine1Number();
        //if(phoneNum == null || phoneNum.equals("")){ phoneNumFlag = true; }
        //if(operator == null || operator.equals("")){ operatorFlag = true; }
        if (telManager.getSimState() == TelephonyManager.SIM_STATE_ABSENT){
            Log.d(TAG, "유심 x");
            usimFlag = true;
        } else {
            Log.d(TAG, "유심 o");
            usimFlag = false;
        }

        Log.d(TAG, "contactsFlag : " + contactsFlag);
        Log.d(TAG, "usimFlag : " + usimFlag);
        Log.d(TAG, "isPhone : " + isPhone);
        //Log.d(TAG, "phoneNumFlag : " + phoneNumFlag);
        if ((contactsFlag || usimFlag) && isPhone) {
            view = inflater.inflate(R.layout.depend, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment20, container, false);
            gridView = (GridView)view.findViewById(R.id.gridview20);

            editText = (EditText)view.findViewById(R.id.fr20_edit);
            searchBtn = (Button)view.findViewById(R.id.fr20_searchbtn);
            searchBtn.setOnClickListener(this);

            tv_currentPage = (TextView)view.findViewById(R.id.fr20_currentpage);
            tv_lastPage = (TextView)view.findViewById(R.id.fr20_lastpage);
            preBtn = (Button)view.findViewById(R.id.fr20_prebtn);
            nextBtn = (Button)view.findViewById(R.id.fr20_nextbtn);

            preBtn.setOnClickListener(this);
            nextBtn.setOnClickListener(this);

            inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr20_searchbtn :
                if(!editText.getText().toString().equals("")){
                    searchWord = editText.getText().toString();
                    inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    pageNum = 1;
                    new GetGridView().execute();
                } else {
                    Toast.makeText(getActivity(), "검색어를 입력하세요.", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.fr20_prebtn :
                if(pageNum != 1){
                    pageNum--;
                    //asyncGridview.execute();
                    new GetGridView().execute();
                }
                break;

            case R.id.fr20_nextbtn :
                if(pageNum != Integer.parseInt(tv_lastPage.getText().toString())){
                    pageNum++;
                    //asyncGridview.execute();
                    new GetGridView().execute();
                }
                break;
        }
    }

    public class GetGridView extends AsyncTask<Void, Void, Void> {

        String tempLastPage = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
            tv_currentPage.setText("");
            tv_lastPage.setText("");
        }

        @Override
        protected Void doInBackground(Void... params) {

            gridArr = null;
            gridArr = new ArrayList<GridViewItem>();

            Document doc = null;
Log.d(TAG, "fullUrl : " + baseUrl + searchPagingUrl + pageNum + attrUrl + searchWord);
            try {
                doc = Jsoup.connect(baseUrl + searchPagingUrl + pageNum + attrUrl + searchWord).timeout(15000).userAgent("Chrome").get();


                Elements table = doc.select(".video-section");
                Elements divs = table.select(".item");

                /*Elements div = doc.select(".video-section div.item");*/
                Elements pages = doc.select(".pagination li");
                /*Log.d(TAG, )*/
                for(int i=0 ; i<divs.size() ; i++){
                    String title = divs.get(i).select("h3").text();
                    String imgUrl = divs.get(i).select("img").attr("src");
                    String listUrl = divs.get(i).select("a").attr("href");
                    String updateDt = divs.get(i).select(".meta .date").text();

                    Log.d(TAG, title);
                    Log.d(TAG, imgUrl);
                    Log.d(TAG, listUrl);
                    Log.d(TAG, updateDt);

                    gridArr.add(new GridViewItem(imgUrl, title, listUrl, updateDt));

                }

                if(pages.get(pages.size()-1).text().equals("다음 →")){
                    tempLastPage = pages.get(pages.size()-2).text();
                } else {
                    tempLastPage = pages.get(pages.size()-1).text();
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            tv_currentPage.setText(pageNum+"");
            tv_lastPage.setText(tempLastPage);

            // adapter에 적용
            gridView.setAdapter(new GridViewAdapter(getActivity(), gridArr, R.layout.gridviewitem));

            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(NetworkUtil.getConnectivity(getActivity())){

                        Intent intent = new Intent(getActivity(), VideoListActivity.class);

                        intent.putExtra("listUrl", gridArr.get(position).getListUrl());
                        intent.putExtra("imgUrl", gridArr.get(position).getImgUrl());
                        intent.putExtra("title", gridArr.get(position).getTitle());
                        startActivity(intent);
                    } else {
                        Toast.makeText(getActivity(), "check your network connection status", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getGridView != null){
            getGridView.cancel(true);
        }

    }

}
