package com.innovalic.finalfreetv.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.innovalic.finalfreetv.R;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-14.
 */
public class MidListViewAdapter extends BaseAdapter {
    Context context;
    private LayoutInflater inflater;
    private ArrayList<String> listArr;

    public MidListViewAdapter(Context context, LayoutInflater inflater, ArrayList<String> listArr) {
        this.context = context;
        this.inflater = inflater;
        this.listArr = listArr;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.listviewitem, null);
        if(convertView != null){


            //ImageView imageView = (ImageView)convertView.findViewById(R.id.listview_img);
            TextView textView = (TextView)convertView.findViewById(R.id.listview_tv);

            String data = listArr.get(position);

            //Picasso.with(context).load(data).into(imageView);
            textView.setText(data);

        }

        return convertView;
    }



    @Override
    public int getCount() {
        return listArr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
