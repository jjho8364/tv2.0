package com.innovalic.finalfreetv.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.innovalic.finalfreetv.R;
import com.innovalic.finalfreetv.model.ListViewItem2;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-07.
 */
public class ListViewAdapter2 extends BaseAdapter {
    Context context;
    private LayoutInflater inflater;
    private ArrayList<ListViewItem2> listArr;

    public ListViewAdapter2(Context context, LayoutInflater inflater, ArrayList<ListViewItem2> listArr) {
        this.context = context;
        this.inflater = inflater;
        this.listArr = listArr;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.listviewitem2, null);
        if(convertView != null){


            //ImageView imageView = (ImageView)convertView.findViewById(R.id.listview_img);
            TextView textView = (TextView)convertView.findViewById(R.id.listview2_tv);

            ListViewItem2 data = listArr.get(position);

            //Picasso.with(context).load(data.getImgUrl()).into(imageView);
            textView.setText(data.getTitle());

        }

        return convertView;
    }



    @Override
    public int getCount() {
        return listArr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
