package com.innovalic.finalfreetv.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.innovalic.finalfreetv.R;
import com.innovalic.finalfreetv.model.GridViewItemMid;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Administrator on 2016-07-24.
 */
public class GridViewMidAdapter extends BaseAdapter {
    String TAG = "GridViewMidAdapterTAG";
    Activity context;
    ArrayList<GridViewItemMid> gridArr;
    LayoutInflater inf;
    int layout;

    private ArrayList<GridViewItemMid> arraylist;

    static class ViewHolder {
        public ImageView imageView;
        public TextView title;
    }

    public GridViewMidAdapter(){

    }

    public GridViewMidAdapter(Activity context, ArrayList<GridViewItemMid> gridArr, int layout) {
        this.context = context;
        this.gridArr = gridArr;
        this.inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layout = layout;
        this.arraylist = new ArrayList<GridViewItemMid>();
        arraylist.addAll(gridArr);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (convertView == null){
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inf.inflate(layout, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.imageView= (ImageView)rowView.findViewById(R.id.gridview_img);
            viewHolder.title = (TextView)rowView.findViewById(R.id.gridview_title);

            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder)rowView.getTag();
        GridViewItemMid data = gridArr.get(position);
        Picasso.with(context).load(data.getImgUrl()).into(holder.imageView);
        holder.title.setText(data.getTitle());

        return rowView;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        gridArr.clear();
        Log.d(TAG, "charText : " + charText);
        if (charText.length() == 0) {
            gridArr.addAll(arraylist);
        }
        else {
            for (GridViewItemMid wp : arraylist) {
                if (wp.getTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
                    gridArr.add(wp);
                }
                /*if (wp.getCountry().toLowerCase(Locale.getDefault()).contains(charText)) {
                    worldpopulationlist.add(wp);
                }*/
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return gridArr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
