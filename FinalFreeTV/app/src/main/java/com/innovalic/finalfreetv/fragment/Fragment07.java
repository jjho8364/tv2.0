package com.innovalic.finalfreetv.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.innovalic.finalfreetv.R;
import com.innovalic.finalfreetv.activity.MainActivity;
import com.innovalic.finalfreetv.activity.MidListActivity;
import com.innovalic.finalfreetv.activity.VideoListActivity;
import com.innovalic.finalfreetv.activity.VideoViewActivity;
import com.innovalic.finalfreetv.adapter.Fr1GridViewAdapter;
import com.innovalic.finalfreetv.adapter.GridViewAdapter;
import com.innovalic.finalfreetv.adapter.GridViewMidAdapter;
import com.innovalic.finalfreetv.model.Fr1GridViewItem;
import com.innovalic.finalfreetv.model.GridViewItem;
import com.innovalic.finalfreetv.model.GridViewItemMid;
import com.innovalic.finalfreetv.utils.NetworkUtil;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-20.
 */
public class Fragment07 extends Fragment implements View.OnClickListener {
    private final String TAG = " Fragment07 - ";
    private ImageView sbs_ori;
    private GridView gridView;
    //private Fr1GridViewAdapter adapter;
    private GridViewMidAdapter adapter;
    private ArrayList<GridViewItemMid> gridArr;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        ((MainActivity)getActivity()).hideHistoryBtn(); // history hide
        int portrait_width_pixel= Math.min(this.getResources().getDisplayMetrics().widthPixels, this.getResources().getDisplayMetrics().heightPixels);
        int dots_per_virtual_inch=this.getResources().getDisplayMetrics().densityDpi;
        boolean isPhone = true;
        boolean contactsFlag = false;
        //boolean phoneNumFlag = false;
        boolean usimFlag = false;
        float virutal_width_inch=portrait_width_pixel/dots_per_virtual_inch;
        if (virutal_width_inch <= 2) { isPhone = true; } else { isPhone = false; }
        Cursor c = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        int contactsCnt = c.getCount();
        if(contactsCnt < 20){ contactsFlag = true; }
        TelephonyManager telManager = (TelephonyManager)getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        //String phoneNum = telManager.getLine1Number();
        //if(phoneNum == null || phoneNum.equals("")){ phoneNumFlag = true; }
        //if(operator == null || operator.equals("")){ operatorFlag = true; }
        if (telManager.getSimState() == TelephonyManager.SIM_STATE_ABSENT){
            Log.d(TAG, "유심 x");
            usimFlag = true;
        } else {
            Log.d(TAG, "유심 o");
            usimFlag = false;
        }

        Log.d(TAG, "contactsFlag : " + contactsFlag);
        Log.d(TAG, "usimFlag : " + usimFlag);
        Log.d(TAG, "isPhone : " + isPhone);
        //Log.d(TAG, "phoneNumFlag : " + phoneNumFlag);
        if ((contactsFlag || usimFlag) && isPhone) {
            view = inflater.inflate(R.layout.depend, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment05, container, false);if(NetworkUtil.getConnectivity(getActivity())){
                //final ArrayList<Fr1GridViewItem> gridArr = new ArrayList<Fr1GridViewItem>();
                gridArr = new ArrayList<GridViewItemMid>();
                gridArr.add(new GridViewItemMid("https://tv.pstatic.net/thm?size=120x172&quality=8&q=http://sstatic.naver.net/keypage/image/dss/57/0/0/161/161417.jpg","커피프린스 1호점"));




                gridView = (GridView)view.findViewById(R.id.gridview);
                adapter = new GridViewMidAdapter(getActivity(), gridArr, R.layout.gridviewitem_mid);
                gridView.setAdapter(adapter);
                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent(getActivity(), MidListActivity.class);
                        intent.putExtra("title", gridArr.get(position).getTitle());
                        intent.putExtra("type", "kr");
                        startActivity(intent);
                    }
                });
            } else {
                Toast.makeText(getActivity(), "check your network connection status", Toast.LENGTH_SHORT).show();
            }

        }

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
        }
    }



    @Override
    public void onDestroy() {
        super.onDestroy();


    }
}
